/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <object.h>
#include <util.h>
#include <api/faults.h>
#include <api/types.h>
#include <kernel/cspace.h>
#include <kernel/thread.h>
#include <kernel/vspace.h>
#include <object/schedcontext.h>
#include <model/preemption.h>
#include <model/statedata.h>
#include <arch/machine.h>
#include <arch/machine/hardware.h>
#include <arch/kernel/thread.h>
#include <machine/registerset.h>
#include <linker.h>

static seL4_MessageInfo_t
transferCaps(seL4_MessageInfo_t info,
             endpoint_t *endpoint, tcb_t *receiver,
             word_t *receiveBuffer);

BOOT_CODE void configureIdleThread(tcb_t *tcb)
{
    Arch_configureIdleThread(tcb);
    setThreadState(tcb, ThreadState_IdleThreadState);
}

void activateThread(void)
{
    if (unlikely(NODE_STATE(ksCurThread)->tcbYieldTo)) {
        schedContext_completeYieldTo(NODE_STATE(ksCurThread));
        assert(thread_state_get_tsType(NODE_STATE(ksCurThread)->tcbState) == ThreadState_Running);
    }

    switch (thread_state_get_tsType(NODE_STATE(ksCurThread)->tcbState)) {
    case ThreadState_Running:
#ifdef CONFIG_VTX
    case ThreadState_RunningVM:
#endif
        break;

    case ThreadState_Restart: {
        word_t pc;

        pc = getRestartPC(NODE_STATE(ksCurThread));
        setNextPC(NODE_STATE(ksCurThread), pc);
        setThreadState(NODE_STATE(ksCurThread), ThreadState_Running);
        break;
    }

    case ThreadState_IdleThreadState:
        Arch_activateIdleThread(NODE_STATE(ksCurThread));
        break;

    default:
        fail("Current thread is blocked");
    }
}

void suspendCommon(tcb_t *target, word_t tsType)
{
    if (thread_state_get_tsType(target->tcbState) == ThreadState_Running) {
        /* whilst in the running state it is possible that restart pc of a thread is
         * incorrect. As we do not know what state this thread will transition to
         * after we make it inactive we update its restart pc so that the thread next
         * runs at the correct address whether it is restarted or moved directly to
         * running */
        updateRestartPC(target);
    }
    setThreadState(target, tsType);
    tcbSchedDequeue(target);
    tcbReleaseRemove(target);
    schedContext_cancelYieldTo(target);
}

bool_t checkPause(tcb_t *tptr, bool_t updateRestart){
    if (tptr->pauseOnIPCCompletion){
        ipc_debug_printf("checkPause: pausing %p\n", tptr);
        tptr->pauseOnIPCCompletion = false;
        if (updateRestart) {
            updateRestartPC(tptr);
        }
        pause(tptr);
        return true;
    }else{
        return false;
    }
}

bool_t pause(tcb_t *target)
{
    if (!pauseIPC(target)){
        return true;
    }
    seL4_Word origState = thread_state_get_tsType(target->tcbState);
    bool_t pauseRequired = origState != ThreadState_Paused && origState != ThreadState_Inactive;
    if (pauseRequired){
        ipc_debug_printf("pause required\n");
        suspendCommon(target, ThreadState_Paused);
    }
    return pauseRequired;
}

bool_t suspend(tcb_t *target)
{
    ipc_debug_printf("suspending thread %p\n", target);
    cancelIPC(target);
    seL4_Word origState = thread_state_get_tsType(target->tcbState);
    suspendCommon(target, ThreadState_Inactive);
    target->firstPhaseComplete = false;
    return origState != ThreadState_Inactive;
}

void restart(tcb_t *target)
{
    ipc_debug_printf("restarting thread %p\n", target);
    if (target->pauseOnIPCCompletion){
        ipc_debug_printf("unsetting pause on completion flag\n");
        target->pauseOnIPCCompletion = false;
        return;
    }
    if (isStopped(target)) {
        ipc_debug_printf("resume required: %lx\n", getRestartPC(target));
        softCancelIPC(target);
        setThreadState(target, ThreadState_Restart);
        if (sc_sporadic(target->tcbSchedContext) && sc_active(target->tcbSchedContext)
            && target->tcbSchedContext != NODE_STATE(ksCurSC)) {
            refill_unblock_check(target->tcbSchedContext);
        }
        schedContext_resume(target->tcbSchedContext);
        if (isSchedulable(target)) {
            possibleSwitchTo(target);
        }
    }
}

exception_t doIPCTransfer(bool_t senderRunning, bool_t senderInitiated,
                    bool_t sendRemoteFault, bool_t is_call, tcb_t *sender,
                    long_vector_t *sendLongVector, endpoint_t *endpoint,
                    word_t badge, bool_t grant, tcb_t *receiver,
                    long_vector_t *receiveLongVector,
                    long_vector_t *saveReplyLongVector)
{
    void *receiveBuffer, *sendBuffer;

    receiveBuffer = lookupIPCBuffer(true, receiver);

    if (likely(seL4_Fault_get_seL4_FaultType(sender->tcbFault) == seL4_Fault_NullFault)) {
        sendBuffer = lookupIPCBuffer(false, sender);
        return (doNormalTransfer(senderRunning, senderInitiated,
                         sendRemoteFault, is_call, sender, sendBuffer,
                         sendLongVector, endpoint, badge, grant, receiver,
                         receiveBuffer, receiveLongVector,
                         saveReplyLongVector));
    } else {
        doFaultTransfer(badge, sender, receiver, receiveBuffer);
    }
    return (EXCEPTION_NONE);
}

exception_t doReplyTransfer(tcb_t *sender, reply_t *reply, bool_t grant, bool_t isLong, bool_t firstPhase)
{
    ipc_debug_printf("doReplyTransfer: %p %p\n", sender, reply);
    exception_t status = EXCEPTION_NONE;
    if (sender->tcbIPCPartner == NULL && (reply->replyTCB == NULL ||
        thread_state_get_tsType(reply->replyTCB->tcbState) != ThreadState_BlockedOnReply)) {
        ipc_debug_printf("no reply pending: %p %p\n", sender, reply);
        /* nothing to do */
        current_syscall_error.type = seL4_ReplySequenceError;
        transferFailed(sender);
        setThreadState(sender, ThreadState_Running);
        return (EXCEPTION_SYSCALL_ERROR);
    }

    tcb_t *receiver;
    sched_context_t *context = NULL;
    tcb_t *context_dest = NULL;
    if (sender->tcbIPCPartner != NULL){
        receiver = sender->tcbIPCPartner;
        context = sender->tcbReplySchedContext;
        context_dest = sender->tcbReplySchedContextDest;
    }else{
        receiver = reply->replyTCB;
        reply_remove(reply, receiver, &context, &context_dest);
        sender->tcbReplySchedContext = context;
        sender->tcbReplySchedContextDest = context_dest;

        assert(thread_state_get_replyObject(receiver->tcbState) == REPLY_REF(0));
        assert(reply->replyTCB == NULL);
        if (isLong){
            sender->longIPCFlags = seL4_MessageInfo_get_capsUnwrapped(messageInfoFromWord(getRegister(sender, msgInfoRegister)));
            receiver->longIPCFlags = seL4_MessageInfo_get_capsUnwrapped(messageInfoFromWord(getRegister(receiver, msgInfoRegister)));
        }
    }

    word_t fault_type = seL4_Fault_get_seL4_FaultType(receiver->tcbFault);

    if (sc_sporadic(receiver->tcbSchedContext) && sc_active(receiver->tcbSchedContext)
        && receiver->tcbSchedContext != NODE_STATE_ON_CORE(ksCurSC, receiver->tcbSchedContext->scCore)) {
        refill_unblock_check(receiver->tcbSchedContext);
    }

    if (likely(fault_type == seL4_Fault_NullFault)) {
        if (isLong && sender->longIPCState != LongIPCState_CopyRunning && sender->longIPCState != LongIPCState_Unrecoverable){
            sender->longIPCState = LongIPCState_Blocked;
        }
        if (receiver->longIPCState == LongIPCState_Inactive){
            isLong = false;
        }
        status = doIPCTransfer(true, true, true, false, sender, &sender->primaryLongVector, NULL, 0, grant, receiver, &receiver->replyLongVector, &sender->replyLongVector);
        if (status != EXCEPTION_NONE && status != EXCEPTION_SYSCALL_ERROR){
            return (status);
        }

        if (likely(status == EXCEPTION_NONE)){
            replyFromKernel_success_empty_send(sender);
        }else{
            transferFailed(sender);
        }

        if (context != NULL) {
            schedContext_donate(context, context_dest);
            sender->tcbReplySchedContext = NULL;
            sender->tcbReplySchedContextDest = NULL;
        }
        if (isLong && status != EXCEPTION_SYSCALL_ERROR){
            ipc_debug_printf("doReplyTransfer: setting reply size\n");
            void *receiveBuffer = lookupIPCBuffer(true, receiver);
            setLongVectorInfo(receiveBuffer, LongBuf_ReceiveSize, receiver->replyLongVector.maxTransferred);
        }

        word_t replyVectorAddress = sender->replyLongVector.address;
        word_t replyVectorCount = sender->replyLongVector.count;
        ipc_debug_printf("reply done\n");

        if (firstPhase){
            sender->primaryLongVector.address = replyVectorAddress;
            sender->primaryLongVector.count = replyVectorCount;
        }else{
            ipc_debug_printf("doReplyTransfer: resetting sender vectors");
            resetLongVectors(sender);
        }
        ipc_debug_printf("doReplyTransfer: resetting IPC state\n");
        resetLongVectors(receiver);
        resetLongIPCState(sender);
        resetLongIPCState(receiver);
        if (!checkPause(receiver, true)){
            setThreadState(receiver, ThreadState_Running);
        }
    } else {
        if (context != NULL) {
            schedContext_donate(context, context_dest);
            sender->tcbReplySchedContext = NULL;
            sender->tcbReplySchedContextDest = NULL;
        }

        bool_t restart = handleFaultReply(receiver, sender);
        receiver->tcbFault = seL4_Fault_NullFault_new();
        if (restart) {
            setThreadState(receiver, ThreadState_Restart);
        } else {
            setThreadState(receiver, ThreadState_Inactive);
        }
    }

    if (receiver->tcbSchedContext && isRunnable(receiver)) {
        if ((refill_ready(receiver->tcbSchedContext) && refill_sufficient(receiver->tcbSchedContext, 0))) {
            possibleSwitchTo(receiver);
        } else {
            if (validTimeoutHandler(receiver) && fault_type != seL4_Fault_Timeout) {
                current_fault = seL4_Fault_Timeout_new(receiver->tcbSchedContext->scBadge);
                handleTimeout(receiver);
            } else {
                postpone(receiver->tcbSchedContext);
            }
        }
    }
    return (status);
}

exception_t doNormalTransfer(bool_t senderRunning, bool_t senderInitiated,
                      bool_t sendRemoteFault, bool_t isCall,
                      tcb_t *sender, word_t *sendBuffer,
                      long_vector_t *sendLongVector, endpoint_t *endpoint,
                      word_t badge, bool_t canGrant, tcb_t *receiver,
                      word_t *receiveBuffer, long_vector_t *receiveLongVector,
                      long_vector_t *saveLongVector)
{
    word_t msgTransferred;
    seL4_MessageInfo_t tag;
    exception_t status;
    bool_t doLongIPC = sender->longIPCState != LongIPCState_Inactive && receiver->longIPCState != LongIPCState_Inactive;
    bool_t doCapTransfer;

    ipc_debug_printf("doNormalTransfer %p %p %ld %ld\n", sender, receiver, sender->longIPCState, receiver->longIPCState);

    tag = messageInfoFromWord(getRegister(sender, msgInfoRegister));

    if (canGrant && !doLongIPC) {
        status = lookupExtraCaps(sender, sendBuffer, tag, false);
        if (unlikely(status != EXCEPTION_NONE)) {
            current_extra_caps.excaprefs[0] = NULL;
        }
        doCapTransfer = true;
    } else {
        current_extra_caps.excaprefs[0] = NULL;
        doCapTransfer = false;
    }

    if (doLongIPC && seL4_Fault_get_seL4_FaultType(sender->tcbFault) == seL4_Fault_NullFault){
        if (sender->longIPCState != LongIPCState_CopyRunning &&
                sender->longIPCState != LongIPCState_CopyBlocked){
            ipc_debug_printf("saving message info\n");
            sender->savedMsgInfo = tag;
        }
        status = transferLongMsg(senderRunning, senderInitiated,
                        sendRemoteFault, isCall, false, false, sender,
                        sendBuffer, sendLongVector, 0, receiver, receiveBuffer,
                        receiveLongVector, 0, saveLongVector);
        if (status != EXCEPTION_PREEMPTED){
            ipc_debug_printf("doNormalTransfer: resetting long IPC state\n");
            resetLongIPCState(sender);
            resetLongIPCState(receiver);
            if (status != EXCEPTION_NONE){
                ipc_debug_printf("doNormalTransfer: resetting vectors\n");
                resetLongVectors(sender);
                resetLongVectors(receiver);
            }
        }
        if (status != EXCEPTION_NONE){
            ipc_debug_printf("long IPC exception: %ld\n", status);
            return status;
        }
        if (isCall) {
            sender->longIPCState = LongIPCState_Blocked;
        }
        tag = sender->savedMsgInfo;
    }
    msgTransferred = copyMRs(sender, sendBuffer, receiver,
                             receiveBuffer,
                             seL4_MessageInfo_get_length(tag));

    if (doCapTransfer){
        tag = transferCaps(tag, endpoint, receiver, receiveBuffer);
    }

    tag = seL4_MessageInfo_set_length(tag, msgTransferred);
    setRegister(receiver, msgInfoRegister, wordFromMessageInfo(tag));
    setRegister(receiver, badgeRegister, badge);
    return EXCEPTION_NONE;
}

void doFaultTransfer(word_t badge, tcb_t *sender, tcb_t *receiver,
                     word_t *receiverIPCBuffer)
{
    word_t sent;
    seL4_MessageInfo_t msgInfo;

    sent = setMRs_fault(sender, receiver, receiverIPCBuffer);
    ipc_debug_printf("doFaultTransfer %p %p\n", sender, receiver);
    msgInfo = seL4_MessageInfo_new(
                  seL4_Fault_get_seL4_FaultType(sender->tcbFault), 0, 0, sent);
    setRegister(receiver, msgInfoRegister, wordFromMessageInfo(msgInfo));
    setRegister(receiver, badgeRegister, badge);
}

void resetLongVectors(tcb_t *thread)
{
    ipc_debug_printf("resetLongVectors %p\n", thread);
    thread->primaryLongVector.address = 0;
    thread->primaryLongVector.count = 0;
    thread->primaryLongVector.maxTransferred = 0;

    thread->replyLongVector.address = 0;
    thread->replyLongVector.count = 0;
    thread->replyLongVector.maxTransferred = 0;
}

void resetLongIPCBufState(long_buffer_state_t *state){
    state->currentBufferIndex = 0;
    state->currentBufferAddress = 0;
    state->currentBufferSize = 0;
    state->currentBufferOffset = 0;
    state->totalProcessed = 0;
    state->totalTransferred = 0;
}

void resetLongIPCState(tcb_t *thread)
{
    ipc_debug_printf("resetLongIPCState: %p\n", thread);
    resetLongIPCBufState(&thread->initiatorBufferState);
    resetLongIPCBufState(&thread->targetBufferState);
    thread->longIPCState = LongIPCState_Inactive;
    thread->currentReplyBufferIndex = 0;
    thread->longIPCFlags = 0;
    thread->tcbIPCPartner = NULL;
    thread->isIPCFaultThread = false;
}

static bool_t sendBufferSpaceRemaining(long_buffer_state_t *state, long_vector_t *vector)
{
    if (state->currentBufferIndex >= vector->count){
        return state->currentBufferOffset < state->currentBufferSize;
    }
    return true;
}

exception_t unrecoverableIPCFault(tcb_t *originThread, tcb_t *initiatingThread, tcb_t *targetThread)
{
    syscall_error_type_t originError = originThread == initiatingThread ? seL4_InvalidLocalIPCAddress : seL4_InvalidRemoteIPCAddress;
    ipc_debug_printf("unrecoverableIPCFault %p %p\n", initiatingThread, targetThread);

    current_syscall_error.type = originError;
    transferFailed(initiatingThread);

    setThreadState(initiatingThread, ThreadState_Running);

    resetLongVectors(initiatingThread);
    resetLongIPCState(initiatingThread);

    if (targetThread){
        current_syscall_error.type = originError == seL4_InvalidRemoteIPCAddress ? seL4_InvalidLocalIPCAddress : seL4_InvalidRemoteIPCAddress;
        transferFailed(targetThread);
        resetLongVectors(targetThread);
        resetLongIPCState(targetThread);
        setThreadState(targetThread, ThreadState_Running);
        targetThread->pauseOnIPCCompletion = false;
        current_syscall_error.type = originError;
    }
    return EXCEPTION_SYSCALL_ERROR;
}

static exception_t getLongMsgPage(tcb_t *thread, tcb_t *runningThread, tcb_t *initiatingThread, tcb_t *targetThread, bool_t write, word_t userAddr, word_t maxLen, char **kernelAddr, word_t *len)
{
    exception_t status;
    word_t userPageStart = ROUND_DOWN(userAddr, seL4_PageBits);
    word_t userPageEnd = userPageStart + BIT(seL4_PageBits);
    char *kernelStart;

    ipc_debug_printf("getLongMsgPage: %p\n", thread);
    if (userPageStart > USER_TOP){
        ipc_debug_printf("page above top of user space\n");
        return unrecoverableIPCFault(thread, initiatingThread, targetThread);
    }

    status = Arch_getLongMsgPage(thread, write, userPageStart, &kernelStart);
    if (status != EXCEPTION_NONE){
        if (status == EXCEPTION_LOOKUP_FAULT){
            /*vm_fault_type_t fault;
            if (write) {
                fault = seL4_IPCWriteFault;
            }else{
                fault = seL4_IPCReadFault;
            }*/

            if (runningThread == NULL){
                runningThread = thread;
            }
            runningThread->isIPCFaultThread = thread == runningThread;
            thread_state_ptr_set_tsType(&runningThread->tcbState, ThreadState_Inactive);
            handleIPCVMFault(thread, userAddr);
            handleFaultWithAlternate(runningThread, thread);
            status = EXCEPTION_PREEMPTED;
        }else{
            unrecoverableIPCFault(thread, initiatingThread, targetThread);
        }
        return status;
    }

    *kernelAddr = kernelStart + (userAddr - userPageStart);
    *len = MIN(maxLen, userPageEnd - userAddr);

    return EXCEPTION_NONE;
}

static exception_t getLongMsgBufferInfo(long_buffer_state_t *bufState, tcb_t *originThread, tcb_t *runningThread, tcb_t *initiatingThread, tcb_t *targetThread, long_vector_t *vector)
{
    if (bufState->currentBufferAddress != 0){
        return EXCEPTION_NONE;
    }
    if (bufState->currentBufferIndex >= vector->count){
        bufState->currentBufferAddress = 0;
        bufState->currentBufferOffset = 0;
        bufState->currentBufferSize = 0;
        return EXCEPTION_NONE;
    }

    exception_t status;
    word_t srcStart = vector->address + bufState->currentBufferIndex * sizeof (seL4_LongMsgBuffer);
    word_t remaining = sizeof (seL4_LongMsgBuffer);

    seL4_LongMsgBuffer buf;
    char *destStart = (char *)&buf;

    while (remaining > 0){
        ipc_debug_printf("getLongMsgBufferInfo: start: %lx %p\n", srcStart, destStart);
        char *curPage;
        word_t len;
        status = getLongMsgPage(originThread, runningThread, initiatingThread, targetThread, false, srcStart, remaining, &curPage, &len);
        if (status != EXCEPTION_NONE){
            ipc_debug_printf("getLongMsgPage failed: %ld\n", status);
            return status;
        }
        memcpy(destStart, curPage, len);
        srcStart += len;
        destStart += len;
        remaining -= len;
    }

    bufState->currentBufferAddress = (word_t)buf.base;
    bufState->currentBufferSize = (word_t)buf.len;
    bufState->currentBufferOffset = 0;
    ipc_debug_printf("getLongMsgBufferInfo: %lx %ld\n", bufState->currentBufferAddress, bufState->currentBufferSize);

    return EXCEPTION_NONE;
}

static void incrementBuffer(long_buffer_state_t *state, long_vector_t *vector)
{
    if (state->currentBufferIndex < vector->count){
        if (state->currentBufferOffset >= state->currentBufferSize){
            state->currentBufferIndex++;
            state->currentBufferSize = 0;
            state->currentBufferAddress = 0;
        }
    }
}

static word_t checkRemaining(long_buffer_state_t *state, long_vector_t *vector, word_t offset)
{
    word_t len = state->currentBufferSize - state->currentBufferOffset;
    if (state->totalProcessed < offset){
        word_t end = state->totalProcessed + state->currentBufferSize;
        if (end > offset) {
            len = end - offset;
        }else{
            len = 0;
        }
        state->currentBufferOffset = state->currentBufferSize - len;
        state->totalProcessed += state->currentBufferSize - len;
    }
    return len;
}

exception_t transferLongMsg(bool_t senderRunning, bool_t senderInitiated,
        bool_t sendRemoteFault, bool_t isCall, bool_t forceLong,
        bool_t receiveBufIsSecondary, tcb_t *sender, word_t *sendBuffer,
        long_vector_t *sendLongVector, word_t sendOffset, tcb_t *receiver,
        word_t *receiveBuffer, long_vector_t *receiveLongVector,
        word_t receiveOffset, long_vector_t *saveLongVector)
{
    tcb_t *runningThread;
    tcb_t *initiatingThread;
    tcb_t *targetThread;
    long_buffer_state_t *sendBufState;
    long_buffer_state_t *recvBufState;

    ipc_debug_printf("transferLongMsg %p %p %ld %ld\n", sender, receiver, sender->longIPCState, receiver->longIPCState);
    ipc_debug_printf("receiveUserFlags: %lx\n", receiver->longIPCFlags);

    bool_t getRecvTotalSize = receiver->longIPCFlags & seL4_LongIPC_GetRecvTotalSize;
    bool_t getReplySize = receiver->longIPCFlags & seL4_LongIPC_GetReplySize;

    if (senderInitiated){
        initiatingThread = sender;
        targetThread = receiver;
        sendBufState = &initiatingThread->initiatorBufferState;
        recvBufState = &initiatingThread->targetBufferState;
    }else{
        initiatingThread = receiver;
        targetThread = sender;
        sendBufState = &initiatingThread->targetBufferState;
        recvBufState = &initiatingThread->initiatorBufferState;
    }

    if (!sendRemoteFault){
        targetThread = NULL;
    }

    tcb_t *unrecoverableThread = NULL;
    if (unlikely(sender->longIPCState == LongIPCState_Unrecoverable)){
        if (sender->isIPCFaultThread) {
            unrecoverableThread = sender;
        }else{
            unrecoverableThread = receiver;
        }
    }else if (unlikely(receiver->longIPCState == LongIPCState_Unrecoverable)){
        if (receiver->isIPCFaultThread) {
            unrecoverableThread = receiver;
        }else{
            unrecoverableThread = sender;
        }
    }
    if (unlikely(unrecoverableThread != NULL)){
        return unrecoverableIPCFault(unrecoverableThread, initiatingThread, targetThread);
    }

    if (sender->longIPCFlags & seL4_LongIPC_Optional && receiver->longIPCFlags & seL4_LongIPC_Optional){
        return EXCEPTION_NONE;
    }

    if (senderRunning){
        runningThread = sender;
        sender->longIPCState = LongIPCState_CopyRunning;
        receiver->longIPCState = LongIPCState_CopyBlocked;
    }else{
        runningThread = receiver;
        sender->longIPCState = LongIPCState_CopyBlocked;
        receiver->longIPCState = LongIPCState_CopyRunning;
    }

    sender->tcbIPCPartner = receiver;
    receiver->tcbIPCPartner = sender;

    ipc_debug_printf("transferLongMsg: %p %p %ld %ld\n", sender, receiver, sender->longIPCState, receiver->longIPCState);
    if (sendLongVector->count == 0 && sender->longIPCState != LongIPCState_Inactive){
        ipc_debug_printf("getting send vector\n");
        sendLongVector->address = getLongVectorInfo(sendBuffer, LongBuf_PrimaryAddress);
        sendLongVector->count = getLongVectorInfo(sendBuffer, LongBuf_PrimaryCount);

        sendBufState->totalProcessed = 0;
    }
    if (receiveLongVector->count == 0 && receiver->longIPCState != LongIPCState_Inactive){
        ipc_debug_printf("getting receive vector\n");
        if (receiveBufIsSecondary) {
            receiveLongVector->address = getLongVectorInfo(receiveBuffer, LongBuf_SecondaryAddress);
            receiveLongVector->count = getLongVectorInfo(receiveBuffer, LongBuf_SecondaryCount);
        }else{
            receiveLongVector->address = getLongVectorInfo(receiveBuffer, LongBuf_PrimaryAddress);
            receiveLongVector->count = getLongVectorInfo(receiveBuffer, LongBuf_PrimaryCount);
        }
        if (receiveOffset > receiveLongVector->maxTransferred){
            receiveLongVector->maxTransferred = receiveOffset;
        }
        recvBufState->totalProcessed = 0;
    }

    ipc_debug_printf("sendLongVector: %p %lx %lx\n", sendLongVector, sendLongVector->address, sendLongVector->count);
    ipc_debug_printf("receiveLongVector: %p %lx %lx\n", receiveLongVector, receiveLongVector->address, receiveLongVector->count);
    if (saveLongVector != NULL && saveLongVector->count == 0 && sender->longIPCState != LongIPCState_Inactive){
        saveLongVector->address = getLongVectorInfo(sendBuffer, LongBuf_SecondaryAddress);
        saveLongVector->count = getLongVectorInfo(sendBuffer, LongBuf_SecondaryCount);
    }
    if (saveLongVector != NULL){
        ipc_debug_printf("saveLongVector: %p %lx %lx\n", saveLongVector, saveLongVector->address, saveLongVector->count);
    }else{
        ipc_debug_printf("no save\n");
    }

    if (isCall && getReplySize && saveLongVector->count != 0 && sender->currentReplyBufferIndex < saveLongVector->count && getRecvTotalSize){
        ipc_debug_printf("getting reply size\n");
        while (sender->currentReplyBufferIndex < saveLongVector->count){
            exception_t status;
            status = getLongMsgBufferInfo(sendBufState, sender, runningThread, initiatingThread, targetThread, saveLongVector);
            if (status != EXCEPTION_NONE){
                return status;
            }
            saveLongVector->maxTransferred += sendBufState->currentBufferSize;
            ipc_debug_printf("update reply size: %ld\n", saveLongVector->maxTransferred);
            sendBufState->currentBufferAddress = 0;
            sendBufState->currentBufferSize = 0;
            sendBufState->currentBufferIndex++;
            sender->currentReplyBufferIndex++;
        }
        sendBufState->currentBufferIndex = 0;
    }
    if (sender->longIPCState == LongIPCState_Inactive || sendLongVector->count == 0){
        goto out;
    }
    while (sendBufferSpaceRemaining(sendBufState, sendLongVector)){
        word_t remaining;
        word_t sendRemaining;
        word_t receiveRemaining;
        exception_t status;
        status = getLongMsgBufferInfo(sendBufState, sender, runningThread, initiatingThread, targetThread, sendLongVector);
        if (status != EXCEPTION_NONE){
            return status;
        }

        status = getLongMsgBufferInfo(recvBufState, receiver, runningThread, initiatingThread, targetThread, receiveLongVector);
        if (status != EXCEPTION_NONE){
            return status;
        }

        sendRemaining = checkRemaining(sendBufState, sendLongVector, sendOffset);
        receiveRemaining = checkRemaining(recvBufState, receiveLongVector, receiveOffset);
        ipc_debug_printf("sendRemaining: %ld receiveRemaining: %ld\n", sendRemaining, receiveRemaining);
        remaining = MIN(sendRemaining, receiveRemaining);

        /* if this is a call, continue iterating until the end of the send
         * vector has been reached regardless of whether the end of the receive
         * vector has been reached, in order to determine the total size of the
         * send buffer for potential LongReadBuf/LongWriteBuf calls on the
         * server side; otherwise break here */
        if ((!isCall || !getRecvTotalSize) && recvBufState->currentBufferIndex >= receiveLongVector->count){
            ipc_debug_printf("not getting total size: %ld %ld\n", isCall, getRecvTotalSize);
            break;
        }
        while (remaining > 0){
            char *sendPageStart;
            word_t sendPageLen = 0;
            char *recvPageStart;
            word_t recvPageLen = 0;
            word_t maxPageLen = MIN(BIT(seL4_PageBits), remaining);
            word_t pageLen;

            ipc_debug_printf("getting page from sender\n");
            status = getLongMsgPage(sender, runningThread, initiatingThread, targetThread, false, sendBufState->currentBufferAddress + sendBufState->currentBufferOffset, maxPageLen, &sendPageStart, &sendPageLen);
            if (status != EXCEPTION_NONE){
                ipc_debug_printf("exception when getting sender page: %ld\n", status);
                return status;
            }
            ipc_debug_printf("getting page from receiver\n");
            status = getLongMsgPage(receiver, runningThread, initiatingThread, targetThread, true, recvBufState->currentBufferAddress + recvBufState->currentBufferOffset, maxPageLen, &recvPageStart, &recvPageLen);
            if (status != EXCEPTION_NONE){
                ipc_debug_printf("exception when getting receiver page: %ld\n", status);
                return status;
            }
            pageLen = MIN(sendPageLen,recvPageLen);
            memcpy(recvPageStart, sendPageStart, pageLen);
            sendBufState->currentBufferOffset += pageLen;
            recvBufState->currentBufferOffset += pageLen;
            recvBufState->totalProcessed += pageLen;
            recvBufState->totalTransferred += pageLen;
            sendLongVector->maxTransferred += pageLen;
            remaining -= pageLen;
            if (recvBufState->totalProcessed > receiveLongVector->maxTransferred){
                ipc_debug_printf("updated receive size\n");
                receiveLongVector->maxTransferred = recvBufState->totalProcessed;
            }
            ipc_debug_printf("update sizes: %ld %ld\n", sendLongVector->maxTransferred, receiveLongVector->maxTransferred);
            status = preemptionPoint();
            if (status != EXCEPTION_NONE){
                ipc_debug_printf("copy preempted with status %ld\n", status);
                return status;
            }
        }

        if (recvBufState->currentBufferIndex >= receiveLongVector->count){
            word_t remaining = sendBufState->currentBufferSize;
            if (sendBufState->currentBufferOffset < sendBufState->currentBufferSize){
                remaining -= sendBufState->currentBufferOffset;
            }
            ipc_debug_printf("update excess send buffer size: %ld %ld %ld %ld\n", sendBufState->currentBufferSize, sendBufState->currentBufferOffset, sendLongVector->maxTransferred, remaining);
            sendLongVector->maxTransferred += remaining;
            sendBufState->currentBufferOffset = sendBufState->currentBufferSize;
        }

        incrementBuffer(sendBufState, sendLongVector);
        incrementBuffer(recvBufState, receiveLongVector);
    }
out:
    ipc_debug_printf("sendLongVector->maxTransferred: %ld\n", sendLongVector->maxTransferred);
    ipc_debug_printf("receiveLongVector->maxTransferred: %ld\n", receiveLongVector->maxTransferred);
    return EXCEPTION_NONE;
}

/* Like getReceiveSlots, this is specialised for single-cap transfer. */
static seL4_MessageInfo_t transferCaps(seL4_MessageInfo_t info,
                                       endpoint_t *endpoint, tcb_t *receiver,
                                       word_t *receiveBuffer)
{
    word_t i;
    cte_t *destSlot;

    info = seL4_MessageInfo_set_extraCaps(info, 0);
    info = seL4_MessageInfo_set_capsUnwrapped(info, 0);

    if (likely(!current_extra_caps.excaprefs[0] || !receiveBuffer)) {
        return info;
    }

    destSlot = getReceiveSlots(receiver, receiveBuffer);

    for (i = 0; i < seL4_MsgMaxExtraCaps && current_extra_caps.excaprefs[i] != NULL; i++) {
        cte_t *slot = current_extra_caps.excaprefs[i];
        cap_t cap = slot->cap;

        if (cap_get_capType(cap) == cap_endpoint_cap &&
            EP_PTR(cap_endpoint_cap_get_capEPPtr(cap)) == endpoint) {
            /* If this is a cap to the endpoint on which the message was sent,
             * only transfer the badge, not the cap. */
            setExtraBadge(receiveBuffer,
                          cap_endpoint_cap_get_capEPBadge(cap), i);

            info = seL4_MessageInfo_set_capsUnwrapped(info,
                                                      seL4_MessageInfo_get_capsUnwrapped(info) | (1 << i));

        } else {
            deriveCap_ret_t dc_ret;

            if (!destSlot) {
                break;
            }

            dc_ret = deriveCap(slot, cap);

            if (dc_ret.status != EXCEPTION_NONE) {
                break;
            }
            if (cap_get_capType(dc_ret.cap) == cap_null_cap) {
                break;
            }

            cteInsert(dc_ret.cap, slot, destSlot);

            destSlot = NULL;
        }
    }

    return seL4_MessageInfo_set_extraCaps(info, i);
}

void doNBRecvFailedTransfer(tcb_t *thread)
{
    /* Set the badge register to 0 to indicate there was no message */
    setRegister(thread, badgeRegister, 0);
}

exception_t doLongReplyCopy(tcb_t *thread, reply_t *reply, bool_t read)
{
    exception_t status;
    tcb_t *targetThread = reply->replyTCB;

    ipc_debug_printf("doLongReplyCopy: %ld\n", read);

    if (thread->tcbIPCPartner == NULL && (targetThread == NULL ||
        thread_state_get_tsType(targetThread->tcbState) != ThreadState_BlockedOnReply)) {
        /* nothing to do */
        ipc_debug_printf("doLongReplyCopy: no reply pending\n");
        current_syscall_error.type = seL4_ReplySequenceError;
        transferFailed(thread);
        setThreadState(thread, ThreadState_Running);
        return (EXCEPTION_SYSCALL_ERROR);
    }

    ipc_debug_printf("doLongReplyCopy: starting copy: %p %p %ld\n", thread, targetThread, targetThread->longIPCState);
    setThreadState(thread, ThreadState_Restart);
    seL4_Word offset = getRegister(thread, msgRegisters[0]);

    tcb_t *sender;
    seL4_Word sendOffset = 0;
    tcb_t *receiver;
    seL4_Word receiveOffset = 0;
    long_vector_t *receiveLongVector;
    long_buffer_state_t *receiveBufState;

    bool_t senderRunning;
    bool_t senderInitiated;
    bool_t receiveBufIsSecondary;

    if (read){
        sender = reply->replyTCB;
        receiver = thread;
        senderRunning = false;
        senderInitiated = false;
        sendOffset = offset;
        receiveLongVector = &receiver->primaryLongVector;
        receiveBufIsSecondary = false;
        receiveBufState = &thread->initiatorBufferState;
    }else{
        sender = thread;
        receiver = reply->replyTCB;
        senderRunning = true;
        senderInitiated = true;
        receiveOffset = offset;
        receiveLongVector = &receiver->replyLongVector;
        receiveBufIsSecondary = true;
        receiveBufState = &thread->targetBufferState;
    }

    seL4_Word *sendBuffer = lookupIPCBuffer(false, sender);
    seL4_Word *receiveBuffer = lookupIPCBuffer(false, receiver);

    status = transferLongMsg(senderRunning, senderInitiated, false, false, true, receiveBufIsSecondary, sender, sendBuffer, &sender->primaryLongVector, sendOffset, receiver, receiveBuffer, receiveLongVector, receiveOffset, NULL);
    ipc_debug_printf("status: %ld\n", status);
    setRegister(thread, msgRegisters[1], receiveBufState->totalTransferred);
    if (status != EXCEPTION_NONE && status != EXCEPTION_SYSCALL_ERROR){
        return status;
    }

    ipc_debug_printf("doLongReplyCopy: resetting vectors");
    resetLongVectors(thread);
    resetLongIPCState(thread);
    resetLongIPCState(targetThread);
    targetThread->longIPCState = LongIPCState_Blocked;

    setThreadState(thread, ThreadState_Running);
    ipc_debug_printf("copy done: %ld\n", status);
    if (unlikely(status != EXCEPTION_NONE)){
        transferFailed(thread);
    }
    checkPause(thread, false);
    return status;
}

static void nextDomain(void)
{
    ksDomScheduleIdx++;
    if (ksDomScheduleIdx >= ksDomScheduleLength) {
        ksDomScheduleIdx = 0;
    }
    NODE_STATE(ksReprogram) = true;
    ksWorkUnitsCompleted = 0;
    ksCurDomain = ksDomSchedule[ksDomScheduleIdx].domain;
    ksDomainTime = usToTicks(ksDomSchedule[ksDomScheduleIdx].length * US_IN_MS);
}

static void switchSchedContext(void)
{
    if (unlikely(NODE_STATE(ksCurSC) != NODE_STATE(ksCurThread)->tcbSchedContext)) {
        NODE_STATE(ksReprogram) = true;
        if (sc_constant_bandwidth(NODE_STATE(ksCurThread)->tcbSchedContext)) {
            refill_unblock_check(NODE_STATE(ksCurThread)->tcbSchedContext);
        }

        assert(refill_ready(NODE_STATE(ksCurThread)->tcbSchedContext));
        assert(refill_sufficient(NODE_STATE(ksCurThread)->tcbSchedContext, 0));
    }

    if (NODE_STATE(ksReprogram)) {
        /* if we are reprogamming, we have acted on the new kernel time and cannot
         * rollback -> charge the current thread */
        commitTime();
    }

    NODE_STATE(ksCurSC) = NODE_STATE(ksCurThread)->tcbSchedContext;
}

static void scheduleChooseNewThread(void)
{
    if (ksDomainTime == 0) {
        nextDomain();
    }
    chooseThread();
}

void schedule(void)
{
    awaken();
    checkDomainTime();

    if (NODE_STATE(ksSchedulerAction) != SchedulerAction_ResumeCurrentThread) {
        bool_t was_runnable;
        if (isSchedulable(NODE_STATE(ksCurThread))) {
            was_runnable = true;
            SCHED_ENQUEUE_CURRENT_TCB;
        } else {
            was_runnable = false;
        }

        if (NODE_STATE(ksSchedulerAction) == SchedulerAction_ChooseNewThread) {
            scheduleChooseNewThread();
        } else {
            tcb_t *candidate = NODE_STATE(ksSchedulerAction);
            assert(isSchedulable(candidate));
            /* Avoid checking bitmap when ksCurThread is higher prio, to
             * match fast path.
             * Don't look at ksCurThread prio when it's idle, to respect
             * information flow in non-fastpath cases. */
            bool_t fastfail =
                NODE_STATE(ksCurThread) == NODE_STATE(ksIdleThread)
                || (candidate->tcbPriority < NODE_STATE(ksCurThread)->tcbPriority);
            if (fastfail &&
                !isHighestPrio(ksCurDomain, candidate->tcbPriority)) {
                SCHED_ENQUEUE(candidate);
                /* we can't, need to reschedule */
                NODE_STATE(ksSchedulerAction) = SchedulerAction_ChooseNewThread;
                scheduleChooseNewThread();
            } else if (was_runnable && candidate->tcbPriority == NODE_STATE(ksCurThread)->tcbPriority) {
                /* We append the candidate at the end of the scheduling queue, that way the
                 * current thread, that was enqueued at the start of the scheduling queue
                 * will get picked during chooseNewThread */
                SCHED_APPEND(candidate);
                NODE_STATE(ksSchedulerAction) = SchedulerAction_ChooseNewThread;
                scheduleChooseNewThread();
            } else {
                assert(candidate != NODE_STATE(ksCurThread));
                switchToThread(candidate);
            }
        }
    }
    NODE_STATE(ksSchedulerAction) = SchedulerAction_ResumeCurrentThread;
#ifdef ENABLE_SMP_SUPPORT
    doMaskReschedule(ARCH_NODE_STATE(ipiReschedulePending));
    ARCH_NODE_STATE(ipiReschedulePending) = 0;
#endif /* ENABLE_SMP_SUPPORT */

    switchSchedContext();

    if (NODE_STATE(ksReprogram)) {
        setNextInterrupt();
        NODE_STATE(ksReprogram) = false;
    }
}

void chooseThread(void)
{
    word_t prio;
    word_t dom;
    tcb_t *thread;

    if (numDomains > 1) {
        dom = ksCurDomain;
    } else {
        dom = 0;
    }

    if (likely(NODE_STATE(ksReadyQueuesL1Bitmap[dom]))) {
        prio = getHighestPrio(dom);
        thread = NODE_STATE(ksReadyQueues)[ready_queues_index(dom, prio)].head;
        assert(thread);
        assert(isSchedulable(thread));
        assert(refill_sufficient(thread->tcbSchedContext, 0));
        assert(refill_ready(thread->tcbSchedContext));
        switchToThread(thread);
    } else {
        switchToIdleThread();
    }
}

void switchToThread(tcb_t *thread)
{
    assert(thread->tcbSchedContext != NULL);
    assert(!thread_state_get_tcbInReleaseQueue(thread->tcbState));
    assert(refill_sufficient(thread->tcbSchedContext, 0));
    assert(refill_ready(thread->tcbSchedContext));

#ifdef CONFIG_BENCHMARK_TRACK_UTILISATION
    benchmark_utilisation_switch(NODE_STATE(ksCurThread), thread);
#endif
    Arch_switchToThread(thread);
    tcbSchedDequeue(thread);
    NODE_STATE(ksCurThread) = thread;
}

void switchToIdleThread(void)
{
#ifdef CONFIG_BENCHMARK_TRACK_UTILISATION
    benchmark_utilisation_switch(NODE_STATE(ksCurThread), NODE_STATE(ksIdleThread));
#endif
    Arch_switchToIdleThread();
    NODE_STATE(ksCurThread) = NODE_STATE(ksIdleThread);
}

void setDomain(tcb_t *tptr, dom_t dom)
{
    tcbSchedDequeue(tptr);
    tptr->tcbDomain = dom;
    if (isSchedulable(tptr)) {
        SCHED_ENQUEUE(tptr);
    }
    if (tptr == NODE_STATE(ksCurThread)) {
        rescheduleRequired();
    }
}

void setMCPriority(tcb_t *tptr, prio_t mcp)
{
    tptr->tcbMCP = mcp;
}
void setPriority(tcb_t *tptr, prio_t prio)
{
    switch (thread_state_get_tsType(tptr->tcbState)) {
    case ThreadState_Running:
    case ThreadState_Restart:
        if (thread_state_get_tcbQueued(tptr->tcbState) || tptr == NODE_STATE(ksCurThread)) {
            tcbSchedDequeue(tptr);
            tptr->tcbPriority = prio;
            SCHED_ENQUEUE(tptr);
            rescheduleRequired();
        } else {
            tptr->tcbPriority = prio;
        }
        break;
    case ThreadState_BlockedOnReceive:
    case ThreadState_BlockedOnSend:
        tptr->tcbPriority = prio;
        reorderEP(EP_PTR(thread_state_get_blockingObject(tptr->tcbState)), tptr);
        break;
    case ThreadState_BlockedOnNotification:
        tptr->tcbPriority = prio;
        reorderNTFN(NTFN_PTR(thread_state_get_blockingObject(tptr->tcbState)), tptr);
        break;
    default:
        tptr->tcbPriority = prio;
        break;
    }
}

/* Note that this thread will possibly continue at the end of this kernel
 * entry. Do not queue it yet, since a queue+unqueue operation is wasteful
 * if it will be picked. Instead, it waits in the 'ksSchedulerAction' site
 * on which the scheduler will take action. */
void possibleSwitchTo(tcb_t *target)
{
    if (target->tcbSchedContext != NULL && !thread_state_get_tcbInReleaseQueue(target->tcbState)) {
        if (ksCurDomain != target->tcbDomain
            SMP_COND_STATEMENT( || target->tcbAffinity != getCurrentCPUIndex())) {
            SCHED_ENQUEUE(target);
        } else if (NODE_STATE(ksSchedulerAction) != SchedulerAction_ResumeCurrentThread) {
            /* Too many threads want special treatment, use regular queues. */
            rescheduleRequired();
            SCHED_ENQUEUE(target);
        } else {
            NODE_STATE(ksSchedulerAction) = target;
        }
    }
}

void setThreadState(tcb_t *tptr, _thread_state_t ts)
{
    thread_state_ptr_set_tsType(&tptr->tcbState, ts);
    scheduleTCB(tptr);
}

void scheduleTCB(tcb_t *tptr)
{
    if (tptr == NODE_STATE(ksCurThread) &&
        NODE_STATE(ksSchedulerAction) == SchedulerAction_ResumeCurrentThread &&
        !isSchedulable(tptr)) {
        rescheduleRequired();
    }
}

void postpone(sched_context_t *sc)
{
    tcbSchedDequeue(sc->scTcb);
    tcbReleaseEnqueue(sc->scTcb);
    NODE_STATE_ON_CORE(ksReprogram, sc->scCore) = true;
}

void setNextInterrupt(void)
{
    time_t next_interrupt = NODE_STATE(ksCurTime) +
                            refill_head(NODE_STATE(ksCurThread)->tcbSchedContext)->rAmount;

    if (numDomains > 1) {
        next_interrupt = MIN(next_interrupt, NODE_STATE(ksCurTime) + ksDomainTime);
    }

    if (NODE_STATE(ksReleaseHead) != NULL) {
        next_interrupt = MIN(refill_head(NODE_STATE(ksReleaseHead)->tcbSchedContext)->rTime, next_interrupt);
    }

    setDeadline(next_interrupt - getTimerPrecision());
}

void chargeBudget(ticks_t consumed, bool_t canTimeoutFault)
{
    if (likely(NODE_STATE(ksCurSC) != NODE_STATE(ksIdleSC))) {
        if (isRoundRobin(NODE_STATE(ksCurSC))) {
            assert(refill_size(NODE_STATE(ksCurSC)) == MIN_REFILLS);
            refill_head(NODE_STATE(ksCurSC))->rAmount += refill_tail(NODE_STATE(ksCurSC))->rAmount;
            refill_tail(NODE_STATE(ksCurSC))->rAmount = 0;
        } else {
            refill_budget_check(consumed);
        }

        assert(refill_head(NODE_STATE(ksCurSC))->rAmount >= MIN_BUDGET);
        NODE_STATE(ksCurSC)->scConsumed += consumed;
    }
    NODE_STATE(ksConsumed) = 0;
    if (likely(isSchedulable(NODE_STATE(ksCurThread)))) {
        assert(NODE_STATE(ksCurThread)->tcbSchedContext == NODE_STATE(ksCurSC));
        endTimeslice(canTimeoutFault);
        rescheduleRequired();
        NODE_STATE(ksReprogram) = true;
    }
}

void endTimeslice(bool_t can_timeout_fault)
{
    if (can_timeout_fault && !isRoundRobin(NODE_STATE(ksCurSC)) && validTimeoutHandler(NODE_STATE(ksCurThread))) {
        current_fault = seL4_Fault_Timeout_new(NODE_STATE(ksCurSC)->scBadge);
        handleTimeout(NODE_STATE(ksCurThread));
    } else if (refill_ready(NODE_STATE(ksCurSC)) && refill_sufficient(NODE_STATE(ksCurSC), 0)) {
        /* apply round robin */
        assert(refill_sufficient(NODE_STATE(ksCurSC), 0));
        assert(!thread_state_get_tcbQueued(NODE_STATE(ksCurThread)->tcbState));
        SCHED_APPEND_CURRENT_TCB;
    } else {
        /* postpone until ready */
        postpone(NODE_STATE(ksCurSC));
    }
}

void rescheduleRequired(void)
{
    if (NODE_STATE(ksSchedulerAction) != SchedulerAction_ResumeCurrentThread
        && NODE_STATE(ksSchedulerAction) != SchedulerAction_ChooseNewThread
        && isSchedulable(NODE_STATE(ksSchedulerAction))
       ) {
        assert(refill_sufficient(NODE_STATE(ksSchedulerAction)->tcbSchedContext, 0));
        assert(refill_ready(NODE_STATE(ksSchedulerAction)->tcbSchedContext));
        SCHED_ENQUEUE(NODE_STATE(ksSchedulerAction));
    }
    NODE_STATE(ksSchedulerAction) = SchedulerAction_ChooseNewThread;
}

void awaken(void)
{
    while (unlikely(NODE_STATE(ksReleaseHead) != NULL && refill_ready(NODE_STATE(ksReleaseHead)->tcbSchedContext))) {
        tcb_t *awakened = tcbReleaseDequeue();
        /* the currently running thread cannot have just woken up */
        assert(awakened != NODE_STATE(ksCurThread));
        /* round robin threads should not be in the release queue */
        assert(!isRoundRobin(awakened->tcbSchedContext));
        /* threads should wake up on the correct core */
        SMP_COND_STATEMENT(assert(awakened->tcbAffinity == getCurrentCPUIndex()));
        /* threads HEAD refill should always be >= MIN_BUDGET */
        assert(refill_sufficient(awakened->tcbSchedContext, 0));
        possibleSwitchTo(awakened);
        /* changed head of release queue -> need to reprogram */
        NODE_STATE(ksReprogram) = true;
    }
}
